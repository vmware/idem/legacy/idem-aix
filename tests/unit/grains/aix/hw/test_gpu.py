from dict_tools import data
import pytest
import mock

LSHW_DATA = """
  *-display
       description: VGA compatible controller
       product: Haswell-ULT Integrated Graphics Controller
       vendor: Intel Corporation
       physical id: 2
       bus info: pci@0000:00:02.0
       version: 0b
       width: 64 bits
       clock: 33MHz
       capabilities: msi pm vga_controller bus_master cap_list rom
       configuration: driver=i915 latency=0
       resources: irq:43 memory:b5000000-b53fffff memory:c0000000-cfffffff ioport:6000(size=64)
  *-display UNCLAIMED
       description: 3D controller
       product: GM108M [GeForce 840M]
       vendor: NVIDIA Corporation
       physical id: 0
       bus info: pci@0000:0a:00.0
       version: a2
       width: 64 bits
       clock: 33MHz
       capabilities: pm msi pciexpress bus_master cap_list
       configuration: latency=0
       resources: memory:b3000000-b3ffffff memory:a0000000-afffffff memory:b0000000-b1ffffff ioport:3000(size=128)
"""


@pytest.mark.asyncio
async def test_load_cpuinfo(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": LSHW_DATA})

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.aix.hw.gpu.load_gpudata = hub.grains.aix.hw.gpu.load_gpudata
        await mock_hub.grains.aix.hw.gpu.load_gpudata()

    assert mock_hub.grains.GRAINS.gpus == (
        {
            "model": "Haswell-ULT Integrated Graphics Controller",
            "vendor": "Intel Corporation",
        },
        {"model": "GM108M [GeForce 840M]", "vendor": "NVIDIA Corporation"},
    )
    assert mock_hub.grains.GRAINS.num_gpus == 2
