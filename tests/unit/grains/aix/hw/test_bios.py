from dict_tools import data
import pytest
import mock

LSCFG_DATA = """
   Name:  IBM,8408-E8E
    Model:  IBM,8408-E8E
    Node:  /
    Device Type:  chrp


      System VPD:
        Record Name.................VSYS
        Flag Field..................XXSV
        Brand.......................S0
        Hardware Location Code......U8408.E8E.21C490V
        Machine/Cabinet Serial No...21C490V
        Machine Type and Model......8408-E8E
        System Unique ID (SUID).....0004AC1B4C46
        World Wide Port Name........C05076087F76
        Product Specific.(FV).......SV830_029
        Version.....................ipzSeries
      Physical Location: U8408.E8E.21C490V

     System Firmware:
        Code Level, LID Keyword.....Phyp_1 13402015060180A00701
        Code Level, LID Keyword.....PFW 14262015051581CF0681
        Code Level, LID Keyword.....FSP_Fil 13342015060381E00109
        Code Level, LID Keyword.....FipS_BU 13342015060381E00208
        Microcode Image.............SV830_048 SV830_048 SV830_048
        Microcode Level.............FW830.00 FW830.00 FW830.00
        Microcode Build Date........20150605 20150605 20150605
        Microcode Entitlement Date..20160717
        Hardware Location Code......U8408.E8E.21C490V-Y1
      Physical Location: U8408.E8E.21C490V-Y1
"""


@pytest.mark.asyncio
async def test_load_lscfg(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": LSCFG_DATA})

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.aix.hw.bios.load_lscfg = hub.grains.aix.hw.bios.load_lscfg
        await mock_hub.grains.aix.hw.bios.load_lscfg()

    assert mock_hub.grains.GRAINS.biosversion == "FW830.00"
    assert mock_hub.grains.GRAINS.biosreleasedate == "20150605"
    assert mock_hub.grains.GRAINS.manufacturer == "IBM"
    assert mock_hub.grains.GRAINS.productname == "SV830_048"
    assert mock_hub.grains.GRAINS.serialnumber == "21C490V"


@pytest.mark.asyncio
async def test_load_uuid(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict(
        {"stdout": "os_uuid 01e487b7-faca-48b5-ffff-ffffffffffff N/A True"}
    )

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.aix.hw.bios.load_uuid = hub.grains.aix.hw.bios.load_uuid
        await mock_hub.grains.aix.hw.bios.load_uuid()

    assert mock_hub.grains.GRAINS.uuid == "01e487b7-faca-48b5-ffff-ffffffffffff"
