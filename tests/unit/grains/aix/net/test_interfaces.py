from dict_tools import data
import pytest
import mock

IFCONFIG_DATA = """
en1: flags=1e084863,480<UP,BROADCAST,NOTRAILERS,RUNNING,SIMPLEX,MULTICAST,GROUPRT,64BIT,CHECKSUM_OFFLOAD(ACTIVE),CHAIN>
        inet 10.153.33.12 netmask 0xffff0000 broadcast 10.153.255.255
         tcp_sendspace 262144 tcp_recvspace 262144 rfc1323 1
en0: flags=1e084863,480<UP,BROADCAST,NOTRAILERS,RUNNING,SIMPLEX,MULTICAST,GROUPRT,64BIT,CHECKSUM_OFFLOAD(ACTIVE),CHAIN>
        inet 172.29.155.34 netmask 0xffffc000 broadcast 172.29.191.255
         tcp_sendspace 262144 tcp_recvspace 262144 rfc1323 1
lo0: flags=e08084b,c0<UP,BROADCAST,LOOPBACK,RUNNING,SIMPLEX,MULTICAST,GROUPRT,64BIT,LARGESEND,CHAIN>
        inet 127.0.0.1 netmask 0xff000000 broadcast 127.255.255.255
        inet6 ::1%1/64
         tcp_sendspace 131072 tcp_recvspace 131072 rfc1323 1
"""

ENTSTAT_EN0_DATA = """
-------------------------------------------------------------
ETHERNET STATISTICS (en0) :
Device Type: Virtual I/O Ethernet Adapter (l-lan)
Hardware Address: de:a4:82:52:be:0c
Elapsed Time: 7 days 3 hours 40 minutes 42 seconds
"""

ENTSTAT_EN1_DATA = """
-------------------------------------------------------------
ETHERNET STATISTICS (en1) :
Device Type: Virtual I/O Ethernet Adapter (l-lan)
Hardware Address: de:a4:82:52:be:0d
Elapsed Time: 7 days 3 hours 56 minutes 53 seconds
"""


@pytest.mark.asyncio
async def test_load_gateway(mock_hub, hub):
    mock_hub.exec.cmd.run.side_effect = [
        data.NamespaceDict({"stdout": IFCONFIG_DATA}),
        data.NamespaceDict({"stdout": ENTSTAT_EN1_DATA}),
        data.NamespaceDict({"stdout": ENTSTAT_EN0_DATA}),
        data.NamespaceDict({"stdout": ""}),
    ]

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.aix.net.interfaces.load_interfaces = (
            hub.grains.aix.net.interfaces.load_interfaces
        )
        await mock_hub.grains.aix.net.interfaces.load_interfaces()

    assert mock_hub.grains.GRAINS.hwaddr_interfaces._dict() == {
        "en0": "de:a4:82:52:be:0c",
        "en1": "de:a4:82:52:be:0d",
    }
    assert mock_hub.grains.GRAINS.ip_interfaces._dict() == {
        "en0": ("172.29.155.34",),
        "en1": ("10.153.33.12",),
        "lo0": ("127.0.0.1", "::1"),
    }
    assert mock_hub.grains.GRAINS.ip4_interfaces._dict() == {
        "en0": ("172.29.155.34",),
        "en1": ("10.153.33.12",),
        "lo0": ("127.0.0.1",),
    }
    assert mock_hub.grains.GRAINS.ip6_interfaces._dict() == {"lo0": ("::1",)}
    assert mock_hub.grains.GRAINS.ipv4 == ("10.153.33.12", "127.0.0.1", "172.29.155.34")
    assert mock_hub.grains.GRAINS.ipv6 == ("::1",)
