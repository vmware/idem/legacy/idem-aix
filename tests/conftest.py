import os
import pytest


@pytest.fixture(scope="session")
def hub(hub):
    try:
        if os.uname().sysname == "AIX":
            return hub
    except Exception:
        ...

    pytest.skip("idem-aix only runs on AIX systems")
