from dict_tools import data
import pytest
import mock


@pytest.mark.asyncio
async def test_load_meminfo(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict(
        {"stdout": "Memory Size: 4096 MB"}
    )

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.aix.hw.mem.load_meminfo = hub.grains.aix.hw.mem.load_meminfo
        await mock_hub.grains.aix.hw.mem.load_meminfo()

    assert mock_hub.grains.GRAINS.mem_total == 4096


@pytest.mark.asyncio
async def test_load_swapinfo(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict(
        {
            "stdout": "allocated  =    688128 blocks    used  =   2647 blocks      free  =    685481 blocks"
        }
    )

    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.aix.hw.mem.load_swapinfo = hub.grains.aix.hw.mem.load_swapinfo
        await mock_hub.grains.aix.hw.mem.load_swapinfo()

    assert mock_hub.grains.GRAINS.swap_total == 2752512
